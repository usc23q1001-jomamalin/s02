# Comments
# Comments in Python are done using the "#" symbol
# ctrl + /  - one line comment in Python

"""
although no keybind for multi-line comment,
it is still possible through the use of 3
sets of double quotation marks
"""

# Python Syntax
# Hello World in Python
print("Hello World")

# Indentation
# Where in other programming languages the indentation in code is for readability, the indentation in Python is VERY IMPORTANT

# Variables
# Declaring Variables
age = 18
middle_initial = "C"
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# Data Types in Python
# Strings (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# Numbers (int, float, complex)
num_of_days = 365 #this is an integer
pi_approx = 3.1416
complex_num = 1 + 5j

# Boolean (bool) - for truth values
isLearning = True
isDifficult = False

# Using Variables
print("My name is " + full_name)
print("My age is " + str(age)) #type-casting

# Typecasting
# int() - converts the value to integer value
# float() - converts the value into a float value
# str() - converts the value int string 

print(3.5)
print(int(3.5))
print(int("9861"))

# F-strings, add a lowercase "f" before the string and place variable in {}
print(f"My age is {age}")
print(f"Hi, my name is {full_name} and my age is {age}")

# Operations
# Arithmetic Operators - performs mathematical operations
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4) #remainder
print(2 ** 6) #exponent

# Assignment Operators
num1 = 3
num1 += 4
print(num1)
num1 -= 4
print(num1)
num1 *= 4
print(num1)
num1 /= 4
print(num1)

# Comparison Operator - used to compare values (return a boolean value)
print(1 == 1)
print(5 > 10)
print(5 < 10)
print(1 <= 1)
print(2 >= 3)
print(1 != 1)

# Logical Operators
print(True and False)
print(False or True)
print(not False)

