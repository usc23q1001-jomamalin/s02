# Activity 1.2
print("Activity 1.2")
name = "Janusz Omamalin"
age = 23
occupation = "IT student"
movie = "Jujutsu Kaisen 0"
rating = 75.0
print(f"I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}%")
print("\n")

# Activity 1.3
print("Activity 1.3")
num1 = 5
num2 = 2
num3 = 15
print(f"a. {num1 * num2}")
print(f"b. {num1 < num3}")
num2 += num3
print(f"c. {num2}")